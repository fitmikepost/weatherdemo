//
//  Temperature.swift
//  WeatherDemo
//
//  Created by Mike Post on 2017-11-13.
//  Copyright © 2017 Mike Post. All rights reserved.
//

import Foundation

struct Temperature {
    
    var celsius: Float
    
    init(celsius: Float) {
        self.celsius = celsius
    }
    
    init(fahrenheit: Float) {
        celsius = (fahrenheit - 32) / 1.8
    }
    
    init(kelvin: Float) {
        celsius = kelvin - 273.15
    }
}
