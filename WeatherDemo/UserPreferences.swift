//
//  UserPreferences.swift
//  WeatherDemo
//
//  Created by Mike Post on 2017-11-13.
//  Copyright © 2017 Mike Post. All rights reserved.
//

import Foundation

struct UserPreferences {
    
    let standardDefaults: UserDefaults = UserDefaults.standard
    static let SelectedIDKey = "SelectedID"
    
    /// Sets the last ID the user selected that corresponds to Location.locationID
    func setSelectedID(_ selectedID: Int) {
        
        standardDefaults.set(selectedID,
                             forKey: UserPreferences.SelectedIDKey)
        
        standardDefaults.synchronize()
    }
    
    /// Retrieves the last ID the user selected that corresponds to Location.locationID, from the UserDefaults.
    ///
    /// - Returns: a valid ID, otherwise 0 if none are found.
    func selectedID() -> Int {
        
        let lastID: Int = standardDefaults.integer(forKey: UserPreferences.SelectedIDKey)
        return lastID
    }
    
}
