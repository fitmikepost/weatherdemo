//
//  NetworkManager.swift
//  WeatherDemo
//
//  Created by Mike Post on 2017-11-12.
//  Copyright © 2017 Mike Post. All rights reserved.
//

import Foundation
import UIKit

public typealias SyncCompletion = ((_ response: Any?, _ error: NSError?) -> Void)

/// A wrapper object for making requests using NSURLSession.
class NetworkManager: NSObject, URLSessionDelegate, URLSessionDataDelegate
{
    // MARK: - Properties
    
    private let API_URL: String = "http://api.openweathermap.org/data/2.5/"
    private let API_KEY: String = "APPID=b17004611b59d4309cd9b208b99c62b9"
    private let PATH: String = "weather"
    
    //Define as singleton...
    static let defaultInstance = NetworkManager()
    
    lazy private(set) var requestSession: URLSession = {
        let defaultConfig: URLSessionConfiguration = URLSessionConfiguration.default
        return URLSession(configuration: defaultConfig)
    }()
    
    // MARK: - Constructors
    
    private override init() {
        super.init()
    }
    
    // MARK: - Public functions
    
    /// Retrieves the existing data from the API.
    ///
    /// - completion: contains `NSData` for a successful response, or `NSError` for a failure.
    func requestData(withParam param: String="", _ completion: @escaping SyncCompletion) {
        
        var requestURL: String = "\(API_URL)\(PATH)"
        
        if param.count > 0 {
            requestURL.append(param)
        }
        requestURL.append("&\(API_KEY)")
        
        let allowedCharacters = NSCharacterSet.urlQueryAllowed
        
        if let encodedURL = requestURL.addingPercentEncoding(withAllowedCharacters: allowedCharacters) {
            self.makeRequest(withURL: encodedURL) { (data, error) -> Void in
                //Parse into json...
                var jsonDict: [AnyHashable: Any]?
                
                if let dataToParse: Data = data as? Data
                {
                    jsonDict = self.parseData(data: dataToParse)
                }
                completion(jsonDict, error)
            }
        }
    }

    private func makeRequest(withURL urlString: String, completion: @escaping SyncCompletion)
    {
        if let url: URL = URL(string: urlString)
        {
            let urlRequest: URLRequest = URLRequest(url: url)
            
            self.requestSession.dataTask(with: urlRequest) { (data, response, error) -> Void in
                
                completion(data as Any?, error as NSError?)
                self.stopNetworkingAnimator()
                }.resume()
        }
    }
    
    private func parseData(data: Data) -> [AnyHashable: Any]?
    {
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as! [AnyHashable: Any]
            return json
        }
        catch {
            return nil
        }
    }
    
}

extension NetworkManager {
 
    func startNetworkingAnimator() {
        DispatchQueue.main.async {
            if UIApplication.shared.isNetworkActivityIndicatorVisible == false {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
        }
    }
    
    
    func stopNetworkingAnimator() {
        DispatchQueue.main.async {
            if UIApplication.shared.isNetworkActivityIndicatorVisible == true {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }
    }

}

