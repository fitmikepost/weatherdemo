//
//  DataStoreManager+Delete.swift
//  WeatherDemo
//
//  Created by Mike Post on 2017-11-13.
//  Copyright © 2017 Mike Post. All rights reserved.
//

import Foundation
import CoreData

extension DataStoreManager
{
    func delete(withObjectID objectID: NSManagedObjectID) {
        
        let toDelete = mainMOC.object(with: objectID)
        mainMOC.delete(toDelete)
        
        saveMain()
    }
    
    func deleteAll()
    {
        //Because we've set up a cascade operation on the Location relationship in the model, we only need to delete all MenuPage objects...
        let locations: [Location] = fetchAllLocations()
        
        if locations.count > 0 {
            for loc: Location in locations {
                mainMOC.delete(loc)
                saveMain()
            }
        }
    }
    
}
