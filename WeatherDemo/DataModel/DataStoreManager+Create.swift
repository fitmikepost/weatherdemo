//
//  DataStoreManager+Create.swift
//  WeatherDemo
//
//  Created by Mike Post on 2017-11-13.
//  Copyright © 2017 Mike Post. All rights reserved.
//

import Foundation
import CoreData

extension DataStoreManager
{
    func createLocation(locationID: Int, city: String?, country: String?, latitude: Double, longitude: Double) -> Location? {
        
        guard let newEntity: NSEntityDescription = NSEntityDescription.entity(forEntityName: "Location",
                                                                              in: backgroundMOC)
            else {
                return nil
        }
        
        let newLocation: Location = Location(entity: newEntity,
                                         insertInto: backgroundMOC)
        
        newLocation.locationID = Int64(locationID)
        newLocation.cityName = city
        newLocation.countryAbbreviation = country
        newLocation.latitude = latitude
        newLocation.longitude = longitude
        
        saveBackground()
        return newLocation
    }
    
    func createWeather(location: Location, forecast: String?, sunrise: TimeInterval?, sunset: TimeInterval?, temperature: Float=0) -> Weather? {
        
        guard let newEntity: NSEntityDescription = NSEntityDescription.entity(forEntityName: "Weather",
                                                                              in: backgroundMOC)
            else {
                return nil
        }
        
        let newWeather: Weather = Weather(entity: newEntity,
                                          insertInto: backgroundMOC)
        
        newWeather.location = location
        newWeather.forecastDescription = forecast
        
        if let rise = sunrise {
            newWeather.sunrise = NSDate(timeIntervalSince1970: rise)
        }
        
        if let set = sunset {
            newWeather.sunset = NSDate(timeIntervalSince1970: set)
        }
        
        newWeather.temperature = temperature
        
        saveBackground()
        return newWeather
    }
    
    func createPrecipitation(weather: Weather, humidity: Float=0, rain: Float=0, snow: Float=0) -> Precipitation? {
        
        guard let newEntity: NSEntityDescription = NSEntityDescription.entity(forEntityName: "Precipitation",
                                                                              in: backgroundMOC)
            else {
                return nil
        }
        
        let newPrecipitation: Precipitation = Precipitation(entity: newEntity,
                                                            insertInto: backgroundMOC)
        
        newPrecipitation.weather = weather
        newPrecipitation.humidity = humidity
        newPrecipitation.rain = rain
        newPrecipitation.snow = snow
        
        saveBackground()
        return newPrecipitation
    }
    
}
