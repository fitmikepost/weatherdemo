//
//  DataStoreManager+JSON.swift
//  WeatherDemo
//
//  Created by Mike Post on 2017-11-13.
//  Copyright © 2017 Mike Post. All rights reserved.
//

import Foundation
import CoreData

extension DataStoreManager
{
    
    /// Creates a Location object, and the Weather and Precipitation relationships from the JSON data.
    ///
    /// - Parameter json:
    /// - Returns: 
    func location(fromJSON json: [String: Any]) -> Location? {
        
        var location: Location?
        var country: String?
        
        guard let sys: [String: Any] = json["sys"] as? [String: Any] else {
            return location
        }
        
        if let countryCode: String = sys["country"] as? String {
            country = countryCode
        }
        
        var lat: Double = 0
        var long: Double = 0
        
        if let coords: [String: Any] = json["coord"] as? [String: Any],
            let newLat = coords["lat"] as? Double,
            let newLon = coords["lon"] as? Double {
            lat = newLat
            long = newLon
        }
        
        //Create Location...
        if let city: String = json["name"] as? String,
            let locID: Int = json["id"] as? Int,
            let loc: Location = createLocation(locationID: locID,
                                               city: city,
                                               country: country,
                                               latitude: lat,
                                               longitude: long) {
            location = loc
            
            //Create Weather relationship...
            let mainData: [String: Any]? = json["main"] as? [String: Any]
            
            let sunrise: TimeInterval? = sys["sunrise"] as? TimeInterval
            let sunset: TimeInterval? = sys["sunset"] as? TimeInterval
            var temperature: Float = 0
            var forecast: String?
            
            if let temp: Float = mainData?["temp"] as? Float {
                temperature = Temperature(kelvin: temp).celsius
                
                //Format to 1 decimal place...
                let decimalPlacedTemp = String(format: "%.1f", temperature)
                temperature = Float(decimalPlacedTemp)!
            }
            
            if let weatherArray: [Any] = json["weather"] as? [Any],
                let weatherData: [String: Any] = weatherArray[0] as? [String: Any] {

                forecast = (weatherData["description"] as? String)?.capitalized
            }
            
            if let weather: Weather = createWeather(location: location!,
                                                    forecast: forecast,
                                                    sunrise: sunrise,
                                                    sunset: sunset,
                                                    temperature: temperature) {
                //Create Precipitation relationship...
                let humidity: Float = (mainData?["humidity"] as? Float) ?? 0
                
                if let precip: Precipitation = createPrecipitation(weather: weather,
                                                                   humidity: humidity) {
                    print("End of location creation. Precipitation: \(precip)")
                }
            }
            
        }

        return location
    }
    
}
