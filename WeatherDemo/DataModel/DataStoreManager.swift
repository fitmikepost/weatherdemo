//
//  DataStoreManager.swift
//  WeatherDemo
//
//  Created by Mike Post on 2017-11-13.
//  Copyright © 2017 Mike Post. All rights reserved.
//

import Foundation
import CoreData
import UIKit


/// A wrapper for the NSPersistentContainer and its core data stack.
public class DataStoreManager: NSObject
{
    let persistentContainer: NSPersistentContainer!
    
    /// This should ideally be used in Fetch.
    lazy var mainMOC: NSManagedObjectContext = {
        return self.persistentContainer.viewContext
    }()
    
    /// This should ideally be used in Update.
    lazy var backgroundMOC: NSManagedObjectContext = {
        return self.persistentContainer.newBackgroundContext()
    }()
    
    //MARK: - Init
    
    /// The designated intialiser to optionally initialise with a NSPersistentContainer of choice.
    /// Otherwise the convenience initialiser uses the AppDelegate's NSPersistentContainer.
    ///
    /// - Parameter container: the NSPersistentContainer to initialise with.
    init(container: NSPersistentContainer) {
        
        self.persistentContainer = container
        self.persistentContainer.viewContext.automaticallyMergesChangesFromParent = true
        self.persistentContainer.viewContext.mergePolicy = NSMergePolicy.mergeByPropertyStoreTrump
    }
    
    override convenience init() {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        //Initialise with convenience intialiser...
        self.init(container: appDelegate.persistentContainer)
    }
    
    //MARK: - Save
    
    func saveMain() {
        
        if mainMOC.hasChanges {
            do {
                try mainMOC.save()
            }
            catch {
                print("MAIN MOC SAVE FAILED: \(error)")
            }
        }
    }
    
    func saveBackground() {
        
        if backgroundMOC.hasChanges {
            do {
                try backgroundMOC.save()
            }
            catch {
                print("BACKGROUND MOC SAVE FAILED: \(error)")
            }
        }
    }
    
}

