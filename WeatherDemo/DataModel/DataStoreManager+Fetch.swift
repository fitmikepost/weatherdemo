//
//  DataStoreManager+Fetch.swift
//  WeatherDemo
//
//  Created by Mike Post on 2017-11-13.
//  Copyright © 2017 Mike Post. All rights reserved.
//

import Foundation
import CoreData

extension DataStoreManager
{
    
    func fetchAllLocations(withPredicate predicate: NSPredicate?=nil) -> [Location] {
        
        var locations = [Location]()
        let fetchRequest: NSFetchRequest<Location> = Location.fetchRequest()
        fetchRequest.predicate = predicate
        
        if let result = try? backgroundMOC.fetch(fetchRequest) {
            locations = result
        }
        return locations
    }
    
    func fetchLocation(matchingID locID: Int) -> Location? {
        
        let predicate: NSPredicate = NSPredicate(format: "locationID == %@", NSNumber(value: locID) )
        let results = fetchAllLocations(withPredicate: predicate)
        
        guard let location: Location = results.last else {
            return nil
        }
        return location
    }
    
}
