//
//  WeatherMenuViewController.swift
//  LoblawWeather
//
//  Created by Mike Post on 2017-11-11.
//  Copyright © 2017 Mike Post. All rights reserved.
//

import UIKit
import CoreData


/// Responsible for presenting the WeatherSearchViewController or WeatherForecastViewController, depending upon the state of the app.
class WeatherMenuViewController: UIViewController {
    
    var forecastViewController: WeatherForecastViewController?
    var searchViewController: WeatherSearchViewController?

}

extension WeatherMenuViewController {
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        //Display the forecast if a location has been set, otherwise display the search UI...
        let prefs: UserPreferences = UserPreferences()
        let locID: Int = prefs.selectedID()
        
        if locID > 0 {
            self.forecastViewController?.view.isHidden = true
            updateForecast(fromLocationID: locID)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "WeatherForecastSegue" {
            forecastViewController = segue.destination as? WeatherForecastViewController
            forecastViewController?.view.isHidden = true
        }
        else if segue.identifier == "WeatherSearchSegue" {
            searchViewController = segue.destination as? WeatherSearchViewController
            searchViewController?.delegate = self
        }
    }

    @IBAction func displayLocations(_ sender: Any) {
        performSegue(withIdentifier: "WeatherSearchSegue", sender: sender)
    }
    
    func displayForecast(withID locationID: Int) {
        
        let dataManager: DataStoreManager = DataStoreManager()
        
        //Predicate search on a normalized data model is fast, no need to place this on a background thread - perhaps in future scope.
        if let location: Location = dataManager.fetchLocation(matchingID: locationID) {
            self.forecastViewController?.locationInView = location
            self.forecastViewController?.view.isHidden = false
        }
    }
    
}

extension WeatherMenuViewController {
    
    func updateForecast(fromLocationID locationID: Int) {
        
        let request: NetworkLocationRequest = NetworkLocationRequest()
        
        request.searchRequest(withLocationID: locationID, forLocation: nil) { [weak self] (json, error) in
            
            guard let jsonDict = json else {
                return
            }
            
            let dataManager: DataStoreManager = DataStoreManager()
            
            if let currentLocation: Location = dataManager.fetchLocation(matchingID: locationID) {
                dataManager.delete(withObjectID: currentLocation.objectID)
                
                if let updatedLocation: Location = dataManager.location(fromJSON: jsonDict) {
                    self?.displayForecast(withID: Int(updatedLocation.locationID))
                }
                
            }
        }
    }
    
}

extension WeatherMenuViewController: WeatherSearchViewControllerDelegate {

    func weatherSearchController(_: WeatherSearchViewController, didSelect locationID: Int) {
        
        self.forecastViewController?.view.isHidden = true
        
        searchViewController?.dismiss(animated: true,
                                      completion: nil)
    }
    
}
