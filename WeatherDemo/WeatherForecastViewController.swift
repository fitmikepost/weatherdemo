//
//  WeatherForecastViewController.swift
//  WeatherDemo
//
//  Created by Mike Post on 2017-11-11.
//  Copyright © 2017 Mike Post. All rights reserved.
//

import UIKit
import CoreLocation

class WeatherForecastViewController: UIViewController {

    @IBOutlet weak var locationName: UILabel!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var forecast: UILabel!
    @IBOutlet weak var sunrise: UILabel!
    @IBOutlet weak var sunset: UILabel!
    
    lazy var dateFormatter: DateFormatter = {
        let formatter: DateFormatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.timeStyle = .short
        return formatter
    }()
    
    var locationInView: Location? {
        didSet {
            locationName.text = locationInView?.cityName
            
            if let temp: Float = locationInView?.weather?.temperature {
                temperature.text = "\(temp) C"
            }
            
            if let forecastDesc: String = locationInView?.weather?.forecastDescription {
                forecast.text = forecastDesc
            }
            
            self.view.layoutIfNeeded()
            self.reverseGeocodeSunDates()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        sunrise.isHidden = true
        sunset.isHidden = true
    }
    
}

extension WeatherForecastViewController {
    
    func reverseGeocodeSunDates() {
        
        guard let locationID = self.locationInView?.locationID else {
            return
        }
        
        let geocoding: Geocoding = Geocoding(latitude: locationInView!.latitude, longitude: locationInView!.longitude)
        geocoding.geocodeTimezone { (placemarks, error) in
            
            if let places: [CLPlacemark] = placemarks,
                let placemark: CLPlacemark = places.first {
                
                self.dateFormatter.timeZone = placemark.timeZone
                
                DispatchQueue.main.async {
                    let dataManager: DataStoreManager = DataStoreManager()
                    
                    if let loc: Location = dataManager.fetchLocation(matchingID: Int(locationID)) {
                        self.loadSunDates(withLocation: loc)
                        self.view.layoutIfNeeded()
                    }
                }
                
            }
        }
    }
    
    func loadSunDates(withLocation location: Location) {
        
        if let sunriseDate: NSDate = location.weather?.sunrise {
            let sunriseFormatted = dateFormatter.string(from: sunriseDate as Date)
            sunrise.text = "Sunrise: \(sunriseFormatted)"
            sunrise.isHidden = false
        }
        
        if let sunsetDate: NSDate = location.weather?.sunset {
            let sunsetFormatted = dateFormatter.string(from: sunsetDate as Date)
            sunset.text = "Sunset: \(sunsetFormatted)"
            sunset.isHidden = false
        }
    }
    
}

