//
//  WeatherSearchViewController.swift
//  WeatherDemo
//
//  Created by Mike Post on 2017-11-11.
//  Copyright © 2017 Mike Post. All rights reserved.
//

import UIKit

protocol WeatherSearchViewControllerDelegate: class {
    
    func weatherSearchController(_: WeatherSearchViewController, didSelect locationID: Int)
}

class WeatherSearchViewController: UIViewController {
    
    weak var delegate: WeatherSearchViewControllerDelegate?
    var locationsViewController: WeatherLocationsTableController?
}

extension WeatherSearchViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "WeatherLocationsSegue" {
            locationsViewController = segue.destination as? WeatherLocationsTableController
            locationsViewController?.searchResultsController = SearchResultsTableController()
            locationsViewController?.delegate = self
        }
    }
    
}

extension WeatherSearchViewController: WeatherLocationsTableControllerDelegate {
    
    func location(didSearchWithError error: AlertError) {
        
        locationsViewController?.dismiss(animated: true,
                                         completion: {
                                            if let title: String = error.errorTitle,
                                                let msg: String = error.errorDescription?.capitalized {
                                                
                                                self.displayError(error: nil,
                                                                  withTitle: title,
                                                                  withMessage: msg)
                                            }
        })
    }
    
    func location(_ weatherLocationsController: WeatherLocationsTableController, didSelect locationID: Int) {
        
        let prefs: UserPreferences = UserPreferences()
        prefs.setSelectedID(locationID)
        
        delegate?.weatherSearchController(self, didSelect: locationID)
    }
    
}

