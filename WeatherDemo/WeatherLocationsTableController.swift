//
//  WeatherLocationsTableController.swift
//  WeatherDemo
//
//  Created by Mike Post on 2017-11-12.
//  Copyright © 2017 Mike Post. All rights reserved.
//

import Foundation
import UIKit
import CoreData

protocol WeatherLocationsTableControllerDelegate: class {
    
    func location(didSearchWithError error: AlertError)

    func location(_ weatherLocationsController: WeatherLocationsTableController, didSelect locationID: Int)
}

class WeatherLocationsTableController: UITableViewController, UISearchBarDelegate, UISearchControllerDelegate {
    
    static let cellIdentifier = "searchCellID"
    let dataStore: DataStoreManager = DataStoreManager()
    
    weak var delegate: WeatherLocationsTableControllerDelegate?
    var searchResultsController: SearchResultsTableController = SearchResultsTableController()
    var locationData: [String: Any]?
    var _fetchedResultsController: NSFetchedResultsController<Location>? = nil

    //Lazy var for property dependency injection...
    lazy var searchController: UISearchController = {
        let newSearchController = UISearchController(searchResultsController: self.searchResultsController)
        newSearchController.isActive = true
        newSearchController.dimsBackgroundDuringPresentation = true
        newSearchController.searchBar.sizeToFit()
        newSearchController.searchBar.tintColor = .white
        newSearchController.searchBar.placeholder = "Enter a city name"
        newSearchController.searchBar.searchBarStyle = .minimal

        self.tableView.tableHeaderView = newSearchController.searchBar
        return newSearchController
    }()
}

extension WeatherLocationsTableController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchController.delegate = self
        searchController.searchBar.delegate = self
        
        searchResultsController.delegate = self
    }

}

extension WeatherLocationsTableController: SearchResultsTableDelegate {
    
    func results(_ resultsController: SearchResultsTableController, didSelectRowAt indexPath: IndexPath) {
        
        //Create a new Location object, and dismiss the results view controller...
        if indexPath.row == 0,
            let json = locationData,
            let _ = dataStore.location(fromJSON: json) {
            
            resultsController.dismiss(animated: true,
                                      completion: nil)
        }
    }
}

extension WeatherLocationsTableController {
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let location: Location = fetchedResultsController?.object(at: indexPath) {
            delegate?.location(self, didSelect: Int(location.locationID))
        }
    }
}

extension WeatherLocationsTableController {
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController?.sections?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionInfo = fetchedResultsController?.sections?[section] else {
            return 0
        }
        return sectionInfo.numberOfObjects
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: WeatherLocationsTableController.cellIdentifier, for: indexPath)
        
        if let location: Location = fetchedResultsController?.object(at: indexPath) {
            configureCell(cell, forLocation: location)
        }
        return cell
    }
    
}

extension WeatherLocationsTableController {
    
    // MARK: - Config Cell
    
    func configureCell(_ cell: UITableViewCell, forLocation location: Location) {
        
        if let city: String = location.cityName,
            let country: String = location.countryAbbreviation {
            
            cell.textLabel?.text = "\(city), \(country)"
        }

        if let temp: Float = location.weather?.temperature {
            cell.detailTextLabel?.text = "\(temp) C"
        }
    }
}

extension WeatherLocationsTableController: NSFetchedResultsControllerDelegate {
    
    // MARK: - Fetched results controller
    
    var fetchedResultsController: NSFetchedResultsController<Location>? {
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController
        }
        
        let fetchRequest: NSFetchRequest<Location> = Location.fetchRequest()
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        let sortDescriptor = NSSortDescriptor(key: "cityName", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        _fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                               managedObjectContext: dataStore.mainMOC,
                                                               sectionNameKeyPath: nil,
                                                               cacheName: nil)
        _fetchedResultsController!.delegate = self
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
        
        return _fetchedResultsController
    }
    
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            configureCell(tableView.cellForRow(at: indexPath!)!, forLocation: anObject as! Location)
        case .move:
            configureCell(tableView.cellForRow(at: indexPath!)!, forLocation: anObject as! Location)
            tableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
}

extension WeatherLocationsTableController {
    
    // MARK: - UISearchControllerDelegate
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        
        if let text: String = searchController.searchBar.text,
            text.characters.count > 0 {
            
            let request: NetworkLocationRequest = NetworkLocationRequest()
            let searchString = text.trimmingCharacters(in: CharacterSet.whitespaces)
                        
            request.searchRequest(forLocation: searchString) { [weak self] (json, error) in
                //Handle error or result...
                guard let jsonDict = json else {
                    let error: AlertError = AlertError(errorTitle: "Network Failed",
                                                       errorCode: AlertError.error403,
                                                       description: "Failed to find locations. Please check your network connection.")
                    
                    self?.delegate?.location(didSearchWithError: error)
                    return
                }
                
                if self?.extractErrorCode(fromJSON: jsonDict) == false {
                    self?.reloadResults(fromJSON: jsonDict)
                }
                
            }
        }
    }
    
}

extension WeatherLocationsTableController {
    
    func reloadResults(fromJSON json: [String: Any]) {
        
        locationData = json

        if let city: String = json["name"] as? String,
            let sys: [String: Any] = json["sys"] as? [String: Any],
            let countryCode: String = sys["country"] as? String {

            let resultsController = self.searchController.searchResultsController as! SearchResultsTableController
            resultsController.locationNames = [String](arrayLiteral: "\(city), \(countryCode)")
            resultsController.tableView.reloadData()
        }
    }
    
    func extractErrorCode(fromJSON json: [String: Any]) -> Bool {
        
        //Check for 404 error...
        if let errorCode = json["cod"] as? String,
            Int(errorCode) == AlertError.error404,
            let msg: String = json["message"] as? String {
            
            let error: AlertError = AlertError(errorTitle: "Search Error",
                                               errorCode: Int(errorCode)!,
                                               description: msg)
            
            delegate?.location(didSearchWithError: error)
            return true
        }
        return false
    }
    
}
