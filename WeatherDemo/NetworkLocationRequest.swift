//
//  NetworkLocationRequest.swift
//  WeatherDemo
//
//  Created by Mike Post on 2017-11-12.
//  Copyright © 2017 Mike Post. All rights reserved.
//

import Foundation

public typealias DownloadCompletion = ((_ json: [String: Any]?, _ error: NSError?) -> Void)

class NetworkLocationRequest
{
    let networker: NetworkManager = NetworkManager.defaultInstance
    
    func searchRequest(withLocationID locationID: Int?=0, forLocation locationString: String?, completion: @escaping DownloadCompletion)
    {
        networker.startNetworkingAnimator()
        let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.background)
        
        queue.async(execute: {
            
            var param: String = ""
            
            if let searchString: String = locationString {
                param = "?q=\(searchString)"
            }
            else if let locID = locationID,
                locID > 0 {
                param = "?id=\(locID)"
            }
            
            self.networker.requestData(withParam: param, { (data, error) in
                
                //Check for errors, and update execute error closure if found...
                guard let json: [String: Any] = data as? [String: Any] else {
                    DispatchQueue.main.async(execute: {
                        completion(nil, error)
                    })
                    return
                }
                
                DispatchQueue.main.async(execute: {
                    completion(json, nil)
                    print(json)
                })
                
            })
        })
    }
    
}
