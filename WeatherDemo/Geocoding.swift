//
//  Geocoding.swift
//  WeatherDemo
//
//  Created by Mike Post on 2017-11-13.
//  Copyright © 2017 Mike Post. All rights reserved.
//

import Foundation
import CoreLocation

class Geocoding {
    
    var latitude: Double
    var longitude: Double
    var timezone: TimeZone?
    
    init(latitude: Double, longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
    }
    
    /// Updates the `timezone` var with the timezone from geocoding.
    func geocodeTimezone(completionHandler: @escaping CoreLocation.CLGeocodeCompletionHandler) {
        
        let loc: CLLocation = CLLocation(latitude: latitude, longitude: longitude)
        let geoCoder: CLGeocoder = CLGeocoder()
        
        geoCoder.reverseGeocodeLocation(loc) { (placemarks, error) in
            completionHandler(placemarks, error)
        }
    }
    
}
