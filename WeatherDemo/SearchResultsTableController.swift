//
//  SearchResultsTableController.swift
//  WeatherDemo
//
//  Created by Mike Post on 2017-11-12.
//  Copyright © 2017 Mike Post. All rights reserved.
//

import Foundation
import UIKit

protocol SearchResultsTableDelegate: class {
    
    func results(_ resultsController: SearchResultsTableController, didSelectRowAt indexPath: IndexPath)
}

class SearchResultsTableController: UITableViewController {
    
    static let cellIdentifier = "resultsCellID"
    
    weak var delegate: SearchResultsTableDelegate?
    
    var locationNames = [String]()
}

extension SearchResultsTableController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: SearchResultsTableController.cellIdentifier)
    }
    
}

extension SearchResultsTableController {
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {        
        delegate?.results(self, didSelectRowAt: indexPath)
    }
}

extension SearchResultsTableController {
    
    // MARK: - UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locationNames.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: SearchResultsTableController.cellIdentifier)!
        
        let location = locationNames[indexPath.row]
        configureCell(cell, forLocation: location)
        
        return cell
    }

}

extension SearchResultsTableController {
    
    // MARK: - Config Cell
    
    func configureCell(_ cell: UITableViewCell, forLocation locationName: String) {
        cell.textLabel?.text = locationName
    }
}
