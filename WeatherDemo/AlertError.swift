//
//  AlertError.swift
//  WeatherDemo
//
//  Created by Mike Post on 2017-11-13.
//  Copyright © 2017 Mike Post. All rights reserved.
//

import Foundation

protocol AlertErrorProtocol: LocalizedError {
    
    var errorTitle: String? { get }
    var errorCode: Int { get }
}

struct AlertError: AlertErrorProtocol {
    
    static let error403: Int = 403
    static let error404: Int = 404
    
    var errorTitle: String?
    var errorCode: Int
    var errorDescription: String? { return _description }
    var failureReason: String? { return _description }
    
    private var _description: String
    
    init(errorTitle: String?, errorCode: Int, description: String) {
        
        self.errorTitle = errorTitle ?? "Error"
        self.errorCode = errorCode
        self._description = description
    }
    
}
