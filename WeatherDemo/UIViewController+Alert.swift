//
//  UIViewController+Alert.swift
//  WeatherDemo
//
//  Created by Mike Post on 2017-11-13.
//  Copyright © 2017 Mike Post. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func displayError(error: Error?, withTitle title: String="", withMessage msg: String="")
    {
        var msg: String = "\(msg)"
        
        if let err = error {
            msg.append(String(describing: err.localizedDescription))
        }
        let alertVC: UIAlertController = UIAlertController(title: title,
                                                           message: msg,
                                                           preferredStyle: .alert)
        
        let alertAction: UIAlertAction = UIAlertAction(title: "Ok",
                                                       style: .default) { (action) in }
        
        alertVC.addAction(alertAction)
        
        self.present(alertVC,
                       animated: true,
                       completion: nil)
    }
    
}
